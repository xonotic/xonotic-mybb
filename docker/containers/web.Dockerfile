FROM debian:latest

ENV MYBB_DOWNLOAD_URL=https://resources.mybb.com/downloads/mybb_1811.zip

RUN apt-get update; \
	apt-get install -y apache2 php5 libapache2-mod-php5 php5-mcrypt php5-gd php5-mysql php5-xmlrpc wget unzip mysql-client git; \
	apt-get clean; apt-get autoclean

RUN wget ${MYBB_DOWNLOAD_URL} -O mybb.zip; \
    rm -rf /var/www/html; \
    unzip mybb.zip "Upload/*" -d /var/www/; \
    mv /var/www/Upload /var/www/html; \
    rm -Rf mybb.zip; \
    chown -R www-data:www-data /var/www/html; \
    mv /var/www/html/inc/config.default.php /var/www/html/inc/config.php

RUN mkdir /application
COPY docker/containers/web/wait-for-mysql.sh /application/wait-for-mysql.sh
RUN chmod +x /application/wait-for-mysql.sh

# Add theme
COPY themes/revoxono/images/revoxono /var/www/html/images/revoxono
COPY themes/revoxono/extra/jquery.sceditor.dark.css /var/www/html/jscripts/sceditor/textarea_styles/jquery.sceditor.dark.css
COPY themes/revoxono/extra/dark.css /var/www/html/jscripts/sceditor/editor_themes/dark.css

RUN chmod -R 0777 /var/www/html/cache \
                  /var/www/html/uploads \
                  /var/www/html/uploads/avatars \
                  /var/www/html/admin/backups/ \
                  /var/www/html/inc/languages  \
                  /var/www/html/inc/settings.php \
                  /var/www/html/inc/config.php && \
    chmod 644 /var/www/html/jscripts/sceditor/editor_themes/dark.css \
              /var/www/html/jscripts/sceditor/textarea_styles/jquery.sceditor.dark.css

WORKDIR /var/www/html

# Setup the mybb-theme-bridge
RUN git clone https://github.com/z/mybb-theme-bridge.git && \
    ln -s mybb-theme-bridge/mytb

EXPOSE 80 443