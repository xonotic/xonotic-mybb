#!/bin/bash

set -e

cmd=$@
count_query="SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'mybb_db';"
bburl_query="SELECT value FROM mybb_settings WHERE name='bburl';"

until mysqladmin ping -h "${MYSQL_HOST}" --silent; do
  >&2 echo "MySQL is unavailable - sleeping"
  sleep 1
done

table_count=$(mysql -h ${MYSQL_HOST} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -se "${count_query}")

if [[ ${table_count} -gt 0 ]]; then

    bburl=$(mysql -h ${MYSQL_HOST} -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -se "${bburl_query}")

    if [[ "${bburl}" == "http://localhost" ]]; then
        rm -rf /var/www/html/install
    fi

fi

>&2 echo "MySQL is up - executing command"
exec ${cmd}