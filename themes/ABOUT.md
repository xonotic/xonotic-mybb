# About These Files

MyBB themes use a .xml file which contains all of the stylesheets and meta data about theme. In addition, each theme may have a directory by the same name that contains images for that theme. This directory goes in `images/` on MyBB.

## revoxono

This is a fork of the theme [revolution Gaming](http://community.mybb.com/mods.php?action=view&pid=196), License - GPL Version 3, 29 June 2007.