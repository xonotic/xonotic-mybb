## revoxono

This is a fork of the theme [revolution Gaming](http://community.mybb.com/mods.php?action=view&pid=196), License - GPL Version 3, 29 June 2007.

![revoxono main forum page](revoxono-main.png)

Modifications by -z-, thanks to sev for weighing in on palette.

#### License

GPL Version 3, 29 June 2007
