# xonotic-mybb

dockerized workflow for developing a theme for MyBB

## Requirements

Docker, follow the [docker installation instructions](https://docs.docker.com/engine/installation/) if you don't currently have it installed.

### (Optional) If you don't have docker-compose installed and would like to run it in inside a venv:

```
virtualenv -p /usr/bin/python3 venv
ln -s venv/bin/activate
source activate
pip install -r requirements-dev.txt
```

To run in **development mode**:

```
docker-compose up
```

This starts two containers:

 * db container running mysql
 * web container running apache
 
These are exposed on your host with ports `80, 443, 3306` for their respective services.

You can rebind the local ports in `docker-compose.yml`.

## Installation

Visit `http://localhost`, on first run, you'll have to go through the installer; `localhost` will not work for the database hostname, you need to use the container link name, `db`.

To rephrase, the database settings are as follows:

```
HOSTNAME: db
USERNAME: mybb
PASSWORD: mybb
DATABASE: mybb_db
```

After installation, you'll have to setup the theme. Login to the [Admin CP](http://localhost/admin/index.php), then navigate to [Themes -> Import a Theme](http://localhost/admin/index.php?module=style-themes&action=import).

Select **Local File** and navigate to `themes/` and upload the theme's .xml file, e.g. `revoxono.xml`.

![image depicting the process of importing a theme](media/docs/mybb-import-theme.png)

### Templates

The [mybb-theme-bridge](https://github.com/z/mybb-theme-bridge) has been integrated into this project, enabling developers to use their IDE to develop themes.

To get templates out of the database:

```
# open a shell on the container
docker-compose exec mybb /bin/bash
# inside the container
php tpl.php dump
# back on the host
sudo chown -R $USER:$USER data/templates/
```

After making changes to the template file, the can be synced back into the database with:

```
# inside the container
php tpl.php sync
```

### TODO

- Write a build script to output tagged releases of themes to another repository
- Persist MyBB config (in progress)
- Investigate automated MyBB installation (basically not easy)
