#!/usr/bin/env bash

THEME_NAME="revoxono"
THEME_ROOT=themes/
THEME_VERSION=0.6.2
PACKAGE_NAME=${THEME_NAME}_${THEME_VERSION}.zip

SCRIPT_DIR=$(cd $(dirname $0); pwd)
BUILD_DIR=${SCRIPT_DIR}/build

echo mkdir -p ${BUILD_DIR}

ignored_files=$(cat .gitignore)
cd ${THEME_ROOT} && \
    theme_files=$(find ${THEME_NAME})
zip -r ${PACKAGE_NAME} -p ${theme_files} LICENSE.md README.md ${THEME_NAME}.xml
mv ${PACKAGE_NAME} ${BUILD_DIR}

echo "${PACKAGE_NAME} built"